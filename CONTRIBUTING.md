## <a name="code"></a> Code Style
We follow [Google's JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html) with the addition of rules for TypeScript enforced by [Google TypeScript Style](https://github.com/google/gts).

## <a name="commit"></a> Commit Message Guidelines

We follow the idea of semantic commit messages [[1](https://seesparkbox.com/foundry/semantic_commit_messages)][[2](https://karma-runner.github.io/0.10/dev/git-commit-msg.html)]. This leads to **more
readable messages**.

### Commit Message Format
Each commit message consists of a **type**, a **scope** and a **subject**:

```
<type>(<scope>): <subject>
```

The **type** and **subject** are mandatory and the **scope** of the message is optional.

Any line of the commit message should not be longer than 100 characters! This allows the message to be easier
to read on git tools.

**Examples:**

```
docs(users): add api documentation
fix: add missing property
chore: update dev dependencies to latest version
feat(login): add form validation for username and password
style: add missing semicolon
```

### <a name="type"></a> Type
Please stick to the following **types** in your commit messages:

* **chore**: Mostly maintenance. Changes that affect the build system, ci configuration or external dependencies, etc.
* **docs**: Changes to documentation
* **feat**: A new feature
* **fix**: A bug fix
* **refactor**: Changes like __restructuring code__ or __renaming variables__ that do not change the functionality of the code.
* **style**: Changes to the style of the code like __formatting__, __adding missing semi colons__, etc.
* **test**: Adding missing tests or correcting existing tests

### Scope
The scope should be the part of the application that's affected (optional).

### Subject
The subject contains a succinct description of the change:

* be in imperative, present tense (i.e. "change" instead of "changed" or "changes")
* not have the first letter capitalized
* not end with a `.` (dot)

## <a name="submit"></a> Submission Guidelines

Before submitting your Merge Request (MR) please consider the following guidelines:

1. Look at already [opened or merged](https://gitlab.com/sirimangalo/dpr/merge_requests) MRs in order to prevent duplicate MRs.
1. If you're not a member of this project, fork the repository.
1. Make your changes in a new git branch. Consider adding the [type](#type) separated by a `/`, the issue number and a some describing words to the branch name:

     ```shell
     git checkout -b feat/4711-user-registration
     ```

1. Develop your feature/fix the problem.
1. Follow our [Code Style](#code).
1. Run the tests and linting locally.
1. Commit your changes using a descriptive commit message that follows our
  [commit message guidelines](#commit):

     ```shell
     git commit -am 'feat(user): add registration possibility'
     ```

1. Push your branch:

    ```shell
    git push origin feat/4711-user-registration
    ```

1. Open the project on GitLab and create a MR to `master`.
1. Give the MR a name that follows our [commit message guidelines](#commit) since the name of the MR will be the commit message on the target branch.
1. Mention the issue you're working on with `#4711`. If your MR closes the issue, type `Closes #4711`.
1. Check `Delete source branch when merge request is accepted.` and `Squash commits when merge request is accepted.` unless individual commits need to be preserved.


## <a name="structure"></a> Project Struture
The project is broken up into the following structure.

```
digital-pali-reader
├── README.md
├── CONTRIBUTING.md
├── node_modules
├── package.json
├── .gitignore
├── public
│   ├── favicon.ico
│   ├── index.html
│   └── manifest.json
└── src
    ├── lib
    │   └── my_library.ts
    ├── app
    │   ├── app.scss
    │   ├── app.ts
    │   ├── app.test.ts
    │   └── sub_component
    │       ├── sub_component.scss
    │       ├── sub_component.ts
    │       └── sub_component.test.ts
    ├── index.scss
    ├── index.ts
    ├── logo.svg
    └── serviceWorker.ts
```
### Components
The fundemental building block of React applications is the component.

Components must:
* Be placed in a directory under `src` with the components name.
* Contain the component file `component.js`, styling `component.scss` and test file `component.test.js`
* Be are all lowercase. 
* As required, multiple word components may be broken up by an underscore `sub_component`.
* Not be nested more than 3 levels deep.

### Non-component libraries
Resuable custom library code should be placed in `src/lib`. If the library contains more than one file, those files should be nested under a directory with that files name.

### Public Directory
`public` is a special directory that contains the source HTML file Webpack will inject the transpiled bundle into.

The `public` directory:
* Can be used to include third party styling or analytics (IE Google Analitics) which do not have NPM packages. 
* Can be used for SEO metadata.
* Will be coppied 1-1 without name resolution or minification.
* Can be used for libraries incompatable with Webpack. 

## Tools

Please install the following tools for best development experience

* [dsznajder.es7-react-js-snippets](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets) - For React/Redux snippets 
* [esbenp.prettier-vscode](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) - Automatic code formatting 
* [EditorConfig.EditorConfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) - For code formatting & consistency 
* [dbaeumer.vscode-eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - For show linting errors within VSCode 
* [msjsdiag.debugger-for-chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome) - For F5 debugging experience from VSCode
* [React developer tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en) - Semantic view of React component tree in chrome develope tools.
* [Redux developer tools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en) - View redux activities in chrome developer tools
* To manage multiple versions of Node/NPM on the same machine use the [NVM for Mac/Linux](https://github.com/nvm-sh/nvm) or [NVM for Windows](https://github.com/coreybutler/nvm-windows).

## React primer

If you are new to react, please go through [this tutorial](https://gitlab.com/snippets/1883405). It is specially tuned for Angular developers.
