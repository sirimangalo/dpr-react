import React from 'react';
import * as Router from 'react-router-dom';
import './App.scss';
import 'whatwg-fetch';
import { Main } from '../Main';
import 'bootstrap/dist/css/bootstrap.min.css';

export class App extends React.Component<{}, {}> {
  render(): React.ReactNode {
    return (
      <Router.BrowserRouter>
        <div className="container-fluid App">
          <div className="col d-flex justify-content-center mt-3">
            <div className="card mb-3" style={{ maxWidth: '15rem' }}>
              <div className="card-header App-header">
                <Router.Link to="/" style={{ color: '#FFF' }}>
                  {'TV Series List'}
                </Router.Link>
              </div>
              <div className="card-body">
                <Main />
              </div>
            </div>
          </div>
        </div>
      </Router.BrowserRouter>
    );
  }

  // NOTE: Keep this here as we need to trigger app wide data load.
  componentDidMount = () => {};
}

export default App;
