import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import * as Router from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import { App } from './components/App';
import Tipitaka from './lib/tipitaka';
import { TipitakaSet } from './lib/tipitaka/interfaces';
import { TipitakaHeir, TipitakaNikaya } from './lib/tipitaka/interfaces';

const tipitaka = new Tipitaka(TipitakaSet.myanmar);
// tipitaka.preload();
tipitaka.getNavigation({ hier: TipitakaHeir.m, nikaya: TipitakaNikaya.s });
tipitaka.getSection({ hier: TipitakaHeir.m, nikaya: TipitakaNikaya.s });

ReactDOM.render(
  <Router.BrowserRouter>
    <App />
  </Router.BrowserRouter>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
