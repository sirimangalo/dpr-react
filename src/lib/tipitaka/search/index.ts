import PouchDB from 'pouchdb-browser';
import pako from 'pako';
import axios from 'axios';
import FlexSearch from 'flexsearch';

import { TipitakaSet, TipitakaLocation } from '../interfaces';
import Store from '../store';
import meta from '../meta/myanmar';
import { levelToLocation, locationFromFilename } from '../utility';

export default class Search {
  private db: PouchDB.Database;
  private searchIndex: any;
  private store: Store;
  private set: TipitakaSet;
  private indexMap: (any)[][];

  constructor(store: Store, set: TipitakaSet) {
    this.set = set;
    this.store = store;
    // eslint-disable-next-line @typescript-eslint/camelcase
    this.db = new PouchDB('search_index_' + set, { adapter: 'idb', auto_compaction: true });
    this.searchIndex = FlexSearch.create({ profile: 'memory' });
    this.indexMap = [];
  }

  private async importFromDB(): Promise<boolean> {
    try {
      const { data: indexData } = await this.db.get('index');
      const { data: indexMapData } = await this.db.get('index_map');

      this.searchIndex.import(pako.inflate(indexData, { to: 'string' }));
      this.indexMap = indexMapData;
    } catch (e) {
      const docNotFound = e.name === 'not_found';
      if (!docNotFound) {
        throw e;
      }
      return false;
    }

    return true;
  }

  async load(): Promise<void> {
    if (!(await this.importFromDB())) {
      const { data } = await axios.get(meta.path + '/search.json');

      await this.db.put({ _id: 'index', data: data.index });
      await this.db.put({ _id: 'index_map', data: data.map });

      this.searchIndex.import(pako.inflate(data.index, { to: 'string' }));
      this.indexMap = data.map;
    }
  }

  private recurseIndex(data: any, index: any[], level: number = 1): void {
    const hasChildren = data.children !== undefined;

    if (hasChildren) {
      data.children.forEach((child: any, i: number) => {
        this.recurseIndex(child, [...index, i], level + 1);
      });
    } else {
      const hasBody = data.body !== undefined;

      if (hasBody) {
        this.searchIndex.add(this.indexMap.length, data.body);
        this.indexMap.push(index);
      }
    }
  }

  async index(): Promise<void> {
    if (!(await this.importFromDB())) {
      for (const file of meta.files) {
        const location = locationFromFilename(file);

        const book = await this.store.get(meta.path + '/' + file);

        this.recurseIndex(book, [location.nikaya, location.hier, location.book]);
      }

      const compressed = pako.deflate(this.searchIndex.export(), { to: 'string' });
      await this.db.put({ _id: 'index', data: compressed });
      await this.db.put({ _id: 'index_map', data: this.indexMap });
    }
  }

  async search(term: string, limit: number = 10): Promise<any> {
    const isLoaded = this.indexMap.length > 0;

    if (!isLoaded) {
      await this.load();
    }

    return this.searchIndex.search(term, limit).map((index: number) => {
      const indexLocation = this.indexMap[index];
      return levelToLocation(indexLocation[0], indexLocation[1], indexLocation.slice(2));
    });
  }

  private recurseRegexSearch(
    expression: RegExp,
    data: any,
    limit: number,
    index: any[],
    level: number = 1,
    result: any[] = [],
  ): any {
    if (result.length >= limit) {
      return result;
    }

    const { children, body } = data;

    const hasChildren = children !== undefined;

    if (hasChildren) {
      children.forEach((child: any, i: number) => {
        this.recurseRegexSearch(expression, child, limit, [...index, i], level + 1, result);
      });
    } else {
      const hasBody = body !== undefined;

      if (hasBody && body.search(expression) !== -1) {
        result.push(index);
      }
    }

    return result;
  }

  async regexSearch(term: string, limit: number = 10): Promise<TipitakaLocation[]> {
    const expression = new RegExp(term);

    let result: any[] = [];

    for (const file of meta.files) {
      const location = locationFromFilename(file);

      const book = await this.store.get(meta.path + '/' + file);

      result = result.concat(
        this.recurseRegexSearch(expression, book, limit - result.length, [
          location.nikaya,
          location.hier,
          location.book,
        ]),
      );

      if (result.length >= limit) {
        break;
      }
    }

    return result.map((indexLocation: any) =>
      levelToLocation(indexLocation[0], indexLocation[1], indexLocation.slice(2)),
    );
  }

  async exportIndex(): Promise<any> {
    return JSON.stringify({
      index: pako.deflate(this.searchIndex.export(), { to: 'string' }),
      map: this.indexMap,
    });

    // save result as a browser download
    // const saveData = (function() {
    //   const a = document.createElement('a');
    //   document.body.appendChild(a);
    //   return function(data: string, fileName: string) {
    //     const blob = new Blob([data], { type: 'octet/stream' });
    //     const url = window.URL.createObjectURL(blob);
    //     a.href = url;
    //     a.download = fileName;
    //     a.click();
    //     window.URL.revokeObjectURL(url);
    //   };
    // })();

    // saveData(exportString, 'search.json');
  }

  async destroy(): Promise<void> {
    await this.db.destroy();
  }
}
