import MockAdapter from 'axios-mock-adapter';
import 'fake-indexeddb/auto';

import axios from 'axios';
import Search from './';
import Store from '../store';
import { TipitakaSet } from '../interfaces';

import mockBook from '../test_data/mock_book';

jest.mock('../meta/myanmar', () => require('../test_data/mock_meta'));
jest.mock('../store');

// @ts-ignore
Store.mockImplementation(() => ({
  get: jest.fn(() => mockBook),
}));

const mockSearchExport =
  '{"index":"x®V*Q²6ÕQ*Ì2Òr¡Ì´\\n(#§\\b$d\\bde&\\u0015Ã\\u00199@\\u0011Q^S\\u0002e¦\\u0001ic°!`Fm¬Nu­N´!0\\u0002\\u0011ÆJ±±\\u0000µ\\"\\u0001","map":[["a","a",0,0,0,0,0,0,0],["a","a",0,0,0,0,0,0,1],["a","a",0,0,0,0,1,0,0],["a","a",0,0,0,0,1,0,1]]}';

const axiosMock = new MockAdapter(axios);
axiosMock.onGet().reply(200, mockSearchExport);

describe('Search', () => {
  it('should index book', async () => {
    const storeMock = new Store('test_store');
    const search = new Search(storeMock, TipitakaSet.myanmar);
    await search.index();

    expect(JSON.parse(await search.exportIndex())).toMatchObject({
      index: expect.any(String),
      map: expect.any(Array),
    });

    await search.destroy();
  });

  it('should load index from remote', async () => {
    const storeMock = new Store('test_store');
    const search = new Search(storeMock, TipitakaSet.thai);
    await search.load();

    expect(await search.exportIndex()).toEqual(mockSearchExport);
    expect(axiosMock.history.get.length).toBe(1);

    await search.destroy();
  });

  it('should perform indexed search', async () => {
    const storeMock = new Store('test_store');
    const search = new Search(storeMock, TipitakaSet.thai);
    await search.index();

    const helloLocation = {
      nikaya: 'a',
      hier: 'a',
      book: 0,
      meta: 0,
      volume: 0,
      vagga: 0,
      sutta: 1,
      section: 0,
      paragraph: 0,
    };

    const loremLocation = {
      nikaya: 'a',
      hier: 'a',
      book: 0,
      meta: 0,
      volume: 0,
      vagga: 0,
      sutta: 0,
      section: 0,
      paragraph: 1,
    };

    expect(await search.search('hello')).toEqual([helloLocation]);
    expect(await search.search('lorem')).toEqual([loremLocation]);

    expect(await search.regexSearch('hello')).toEqual([helloLocation]);
    expect(await search.regexSearch('lorem')).toEqual([loremLocation]);

    await search.destroy();
  });
});
