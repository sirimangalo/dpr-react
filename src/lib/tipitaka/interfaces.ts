export enum TipitakaSet {
  myanmar = 'myanmar',
  thai = 'myanmar',
}

export enum TipitakaNikaya {
  v = 'v',
  d = 'd',
  m = 'm',
  s = 's',
  a = 'a',
  k = 'k',
  y = 'y',
  x = 'x',
  b = 'b',
  g = 'g',
}

export enum TipitakaHeir {
  m = 'm',
  a = 'a',
  t = 't',
}

export interface TipitakaLocation {
  [index: string]: string | number | undefined;
  nikaya: TipitakaNikaya;
  book?: number;
  meta?: number;
  volume?: number;
  vagga?: number;
  sutta?: number;
  section?: number;
  hier: TipitakaHeir;
  paragraph?: number;
}

export interface Section {
  paragraphs: { type: number; body: string }[];
  location: TipitakaLocation;
  displayLocation: string[];
}

export interface Navigation {
  nikaya?: string[];
  book?: string[];
  meta?: string[];
  volume?: string[];
  vagga?: string[];
  sutta?: string[];
  section?: string[];
}
