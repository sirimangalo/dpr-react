import * as R from 'ramda';
import Store from './store';
import Search from './search';
import { TipitakaLocation, Navigation, Section, TipitakaSet } from './interfaces';
import { locationToLevel, levelToLocation, LEVELS, filenameFromLocation } from './utility';
import meta from './meta/myanmar';

class Tipitaka {
  private readonly store: Store;
  private readonly searchIndex: Search;
  private readonly set: TipitakaSet;

  constructor(set: TipitakaSet) {
    this.store = new Store('tipitaka_' + set);
    this.searchIndex = new Search(this.store, set);
    this.set = set;
  }

  private async getBookFromLocation(location: TipitakaLocation): Promise<any> {
    return this.store.get(meta.path + '/' + filenameFromLocation(location));
  }

  async preload(): Promise<void> {
    await Promise.all(meta.files.map(file => this.store.get(meta.path + '/' + file)));
  }

  private recurseSection(
    data: any,
    location: TipitakaLocation,
    levelArray: number[],
    index: number[],
    displayLocation: string[],
    level: number = 1,
    result: any[] = [],
  ): Section[] | void {
    const hasChildren = data.children !== undefined;

    if (!hasChildren) {
      return;
    }

    const isLastLevel = level === LEVELS.length - 1;

    if (isLastLevel) {
      result.push({
        paragraphs: R.map(({ paragraphType, body }) => ({ type: paragraphType, body }), data.children),
        displayLocation,
        location: levelToLocation(location.nikaya, location.hier, index),
      });
    } else {
      const levelIndex = levelArray[level];
      const isWildcardLevel = levelIndex === null;

      const nextLevel = (child: any, i: number): any => {
        this.recurseSection(
          child,
          location,
          levelArray,
          [...index, i],
          [...displayLocation, child.name],
          level + 1,
          result,
        );
      };

      if (!isWildcardLevel) {
        nextLevel(data.children[levelIndex], levelIndex);
      } else {
        data.children.forEach((child: any, i: number) => {
          nextLevel(child, i);
        });
      }
    }

    return result;
  }

  async getSection(location: TipitakaLocation): Promise<Section[]> {
    const book = await this.getBookFromLocation(location);
    const levelArray = locationToLevel(location, null);

    const result = this.recurseSection(book, location, levelArray, [levelArray[0]], []);

    // console.log(R.flatten(result));
    console.log(result);
    return result as Section[];
  }

  private recurseNavigation(data: any, levelArray: number[], result: any = {}, level: number = 0): Navigation {
    const { children } = data;
    const levelIndex = levelArray[level];

    const beforeLastLevel = level < LEVELS.length - 2;
    const hasChildren = children !== undefined;

    if (!hasChildren || !beforeLastLevel) {
      return result;
    }

    // push siblings, then recurse to child
    const firstChild = children[0];

    const hasMultiChildren = children.length > 1;
    const firstChildHasName = firstChild.name !== '';

    if (hasMultiChildren || firstChildHasName) {
      result[firstChild.type] = R.map(({ name }) => name, children);
    }
    return this.recurseNavigation(children[levelIndex], levelArray, result, level + 1);
  }

  async getNavigation(location: TipitakaLocation): Promise<Navigation> {
    const { sets } = (meta.index as any)[location.hier];

    const result = this.recurseNavigation(await this.getBookFromLocation(location), locationToLevel(location), {
      nikaya: R.map(({ name }) => name, Object.values(sets as ObjectConstructor)),
      book: R.map(({ name }) => name, sets[location.nikaya].books),
    });

    console.log(result);

    return result;
  }

  async search(term: string, regex: boolean = false, limit: number = 10): Promise<any> {
    return regex ? await this.searchIndex.regexSearch(term, limit) : await this.searchIndex.search(term, limit);
  }
}

export default Tipitaka;
