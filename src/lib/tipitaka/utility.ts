import { TipitakaHeir, TipitakaLocation, TipitakaNikaya } from './interfaces';

export const LEVELS = ['book', 'meta', 'volume', 'vagga', 'sutta', 'section', 'paragraph'];

export const locationToLevel = (location: TipitakaLocation, defaultValue: any = 0): number[] => {
  return [
    location.book || 0,
    location.meta || defaultValue,
    location.volume || defaultValue,
    location.vagga || defaultValue,
    location.sutta || defaultValue,
    location.section || defaultValue,
    location.paragraph || defaultValue,
  ];
};

export const levelToLocation = (nikaya: TipitakaNikaya, hier: TipitakaHeir, level: number[]): TipitakaLocation => {
  const result: TipitakaLocation = { nikaya, hier };

  for (let i = 0; i < level.length; i++) {
    result[LEVELS[i]] = level[i];
  }

  return result;
};

export const filenameFromLocation = (location: TipitakaLocation): string =>
  location.nikaya + ((location.book || 0) + 1).toString() + location.hier + '.json';

export const locationFromFilename = (filename: string): TipitakaLocation => {
  const [name] = filename.split('.');

  const nikaya = name[0] as TipitakaNikaya;
  const hier = name[name.length - 1] as TipitakaHeir;
  let book = 0;

  const regexNumber = name.match(/[0-9]+/);

  if (regexNumber !== null) {
    book = parseInt(regexNumber[0]) - 1;
  }

  return { nikaya, hier, book };
};
