import MockAdapter from 'axios-mock-adapter';
import 'fake-indexeddb/auto';
import axios from 'axios';
import Store from './';

// This sets the mock adapter on the default instance
let axiosMock = new MockAdapter(axios);

const mockData = {
  foo: 'bar',
};

describe('Store', () => {
  beforeEach(async () => {
    axiosMock = new MockAdapter(axios);
  });

  it('should cache data from remote', async () => {
    axiosMock.onGet().reply(200, mockData);

    const store = new Store('test', false);

    // first time requires network call
    expect(await store.get('/test')).toEqual(mockData);

    // second time from cache
    expect(await store.get('/test')).toEqual(mockData);

    // only one remote call
    expect(axiosMock.history.get.length).toBe(1);

    // cleanup
    await store.destroy();
  });

  it('should compress and decompress', async () => {
    axiosMock.onGet().reply(200, mockData);

    const store = new Store('test', true);

    expect(await store.get('/test')).toEqual(mockData);
    expect(await store.get('/test')).toEqual(mockData);

    // cleanup
    await store.destroy();
  });
});
