import PouchDB from 'pouchdb-browser';
import axios from 'axios';
import pako from 'pako';

/*
  Provides a transparent PouchDB cache for remote files
 */
export default class Store {
  private db: PouchDB.Database;
  private readonly useCompression: boolean;

  constructor(name: string, useCompression: boolean = false) {
    // eslint-disable-next-line @typescript-eslint/camelcase
    this.db = new PouchDB(name, { adapter: 'idb', auto_compaction: true });
    this.useCompression = useCompression;
  }

  private static compress(data: any): any {
    return pako.deflate(JSON.stringify(data), { to: 'string' });
  }
  private static decompress(data: any): any {
    return JSON.parse(pako.inflate(data, { to: 'string' }));
  }

  async get(path: string): Promise<any> {
    const { compress, decompress } = Store;
    try {
      const { data } = await this.db.get(path);

      return this.useCompression ? decompress(data) : data;
    } catch (e) {
      const docNotFound = e.name === 'not_found';
      if (!docNotFound) {
        throw e;
      }

      const { data } = await axios.get(path);
      const payload = this.useCompression ? compress(data) : data;

      try {
        await this.db.put({ _id: path, data: payload });
      } catch (e) {
        console.error(e);
      }

      return data;
    }
  }

  async destroy(): Promise<void> {
    await this.db.destroy();
  }
}
