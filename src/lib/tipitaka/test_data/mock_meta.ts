//mock metadata
export default {
  path: '/data/myanmar',
  sets: {
    a: 'Aṅguttara',
  },
  files: ['a1a.json'],
  index: {
    a: {
      name: 'Aṭṭhakathā',
      description: 'Comentaries',
      sets: {
        a: {
          name: 'Aṅguttara',
          books: [
            {
              name: 'ekakanipāta-aṭṭhakathā',
              path: 'a1a',
            },
          ],
        },
      },
    },
  },
};
