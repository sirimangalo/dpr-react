export default {
  type: 'book',
  name: 'mock book',
  children: [
    {
      type: 'meta',
      name: 'mock meta',
      children: [
        {
          type: 'volume',
          name: 'mock volume',
          children: [
            {
              type: 'vagga',
              name: 'mock vagga',
              children: [
                {
                  type: 'sutta',
                  name: 'mock sutta 1',
                  children: [
                    {
                      type: 'section',
                      name: 'mock section 1',
                      children: [
                        {
                          type: 'paragraph',
                          paragraphType: 1,
                          body: 'the quick brown fox',
                        },
                        {
                          type: 'paragraph',
                          paragraphType: 2,
                          body: 'lorem ipsum',
                        },
                      ],
                    },
                  ],
                },
                {
                  type: 'sutta',
                  name: 'mock sutta 2',
                  children: [
                    {
                      type: 'section',
                      name: 'mock section 2',
                      children: [
                        {
                          type: 'paragraph',
                          paragraphType: 3,
                          body: 'hello world',
                        },
                        {
                          type: 'paragraph',
                          paragraphType: 4,
                          body: 'foo bar',
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
    },
  ],
};
