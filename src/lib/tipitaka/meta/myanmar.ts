export default {
  path: '/data/myanmar',
  sets: {
    v: 'Vinaya',
    d: 'Dīgha',
    m: 'Majjhima',
    s: 'Saṃyutta',
    a: 'Aṅguttara',
    k: 'Khuddaka',
    y: 'Abhidhamma',
    x: 'Vism',
    b: 'Abhidh-s',
    g: 'Byākaraṇa',
  },
  files: [
    'a1a.json',
    'a1m.json',
    'a1t.json',
    'a2a.json',
    'a2m.json',
    'a2t.json',
    'a3a.json',
    'a3m.json',
    'a3t.json',
    'a4a.json',
    'a4m.json',
    'a4t.json',
    'a5a.json',
    'a5m.json',
    'a5t.json',
    'a6a.json',
    'a6m.json',
    'a6t.json',
    'a7a.json',
    'a7m.json',
    'a7t.json',
    'a8a.json',
    'a8m.json',
    'a8t.json',
    'a9a.json',
    'a9m.json',
    'a9t.json',
    'a10a.json',
    'a10m.json',
    'a10t.json',
    'a11a.json',
    'a11m.json',
    'a11t.json',
    'b1m.json',
    'b2m.json',
    'd1a.json',
    'd1m.json',
    'd1t.json',
    'd2a.json',
    'd2m.json',
    'd2t.json',
    'd3a.json',
    'd3m.json',
    'd3t.json',
    'g1m.json',
    'g2m.json',
    'g3m.json',
    'g4m.json',
    'g5m.json',
    'k1a.json',
    'k1m.json',
    'k2a.json',
    'k2m.json',
    'k3a.json',
    'k3m.json',
    'k4a.json',
    'k4m.json',
    'k5a.json',
    'k5m.json',
    'k6a.json',
    'k6m.json',
    'k7a.json',
    'k7m.json',
    'k8a.json',
    'k8m.json',
    'k9a.json',
    'k9m.json',
    'k10a.json',
    'k10m.json',
    'k11m.json',
    'k12a.json',
    'k12m.json',
    'k13a.json',
    'k13m.json',
    'k14a.json',
    'k14m.json',
    'k15a.json',
    'k15m.json',
    'k16m.json',
    'k17m.json',
    'k18m.json',
    'k19m.json',
    'k20m.json',
    'k21m.json',
    'm1a.json',
    'm1m.json',
    'm1t.json',
    'm2a.json',
    'm2m.json',
    'm2t.json',
    'm3a.json',
    'm3m.json',
    'm3t.json',
    's1a.json',
    's1m.json',
    's1t.json',
    's2a.json',
    's2m.json',
    's2t.json',
    's3a.json',
    's3m.json',
    's3t.json',
    's4a.json',
    's4m.json',
    's4t.json',
    's5a.json',
    's5m.json',
    's5t.json',
    'v10t.json',
    'v11t.json',
    'v1a.json',
    'v1m.json',
    'v1t.json',
    'v2a.json',
    'v2m.json',
    'v2t.json',
    'v3a.json',
    'v3m.json',
    'v3t.json',
    'v4a.json',
    'v4m.json',
    'v4t.json',
    'v5a.json',
    'v5m.json',
    'v5t.json',
    'v6a.json',
    'v6m.json',
    'v6t.json',
    'v7t.json',
    'v8t.json',
    'v9t.json',
    'x1a.json',
    'x1m.json',
    'x2a.json',
    'x2m.json',
    'y1a.json',
    'y1m.json',
    'y1t.json',
    'y2a.json',
    'y2m.json',
    'y2t.json',
    'y3a.json',
    'y3m.json',
    'y3t.json',
    'y4a.json',
    'y4m.json',
    'y4t.json',
    'y5a.json',
    'y5m.json',
    'y5t.json',
    'y6a.json',
    'y6m.json',
    'y6t.json',
    'y7m.json',
    'y8m.json',
    'y9a.json',
    'y9m.json',
    'y9t.json',
    'y10m.json',
    'y11m.json',
    'y12m.json',
    'y13m.json',
    'y14m.json',
  ],
  index: {
    a: {
      name: 'Aṭṭhakathā',
      description: 'Commentaries',
      sets: {
        a: {
          name: 'Aṅguttara',
          books: [
            {
              name: 'ekakanipāta-aṭṭhakathā',
              path: 'a1a',
            },
            {
              name: 'dukanipāta-aṭṭhakathā',
              path: 'a2a',
            },
            {
              name: 'tikanipāta-aṭṭhakathā',
              path: 'a3a',
            },
            {
              name: 'catukkanipāta-aṭṭhakathā',
              path: 'a4a',
            },
            {
              name: 'pañcakanipāta-aṭṭhakathā',
              path: 'a5a',
            },
            {
              name: 'chakkanipāta-aṭṭhakathā',
              path: 'a6a',
            },
            {
              name: 'sattakanipāta-aṭṭhakathā',
              path: 'a7a',
            },
            {
              name: 'aṭṭhakanipāta-aṭṭhakathā',
              path: 'a8a',
            },
            {
              name: 'navakanipāta-aṭṭhakathā',
              path: 'a9a',
            },
            {
              name: 'dasakanipāta-aṭṭhakathā',
              path: 'a10a',
            },
            {
              name: 'ekādasakanipāta-aṭṭhakathā',
              path: 'a11a',
            },
          ],
        },
        y: {
          name: 'Abhidhamma',
          books: [
            {
              name: 'Dhammasaṅgaṇī-aṭṭhakathā',
              path: 'y1a',
            },
            {
              name: 'Vibhaṅga-aṭṭhakathā',
              path: 'y2a',
            },
            {
              name: 'Dhātukathā-aṭṭhakathā',
              path: 'y3a',
            },
            {
              name: 'Puggalapaññatti-aṭṭhakathā',
              path: 'y4a',
            },
            {
              name: 'Kathāvatthu-aṭṭhakathā',
              path: 'y5a',
            },
            {
              name: 'Yamakappakaraṇa-aṭṭhakathā',
              path: 'y6a',
            },
            {
              name: 'Paṭṭhānappakaraṇa-aṭṭhakathā',
              path: 'y9a',
            },
          ],
        },
        v: {
          name: 'Vinaya',
          books: [
            {
              name: 'pārājikakaṇḍa-aṭṭhakathā',
              path: 'v1a',
            },
            {
              name: 'pācittiya-aṭṭhakathā',
              path: 'v2a',
            },
            {
              name: 'bhikkhunīvibhaṅgavaṇṇanā',
              path: 'v3a',
            },
            {
              name: 'mahāvagga-aṭṭhakathā',
              path: 'v4a',
            },
            {
              name: 'cūḷavagga-aṭṭhakathā',
              path: 'v5a',
            },
            {
              name: 'parivāra-aṭṭhakathā',
              path: 'v6a',
            },
          ],
        },
        s: {
          name: 'Saṃyutta',
          books: [
            {
              name: 'sagāthāvagga-aṭṭhakathā',
              path: 's1a',
            },
            {
              name: 'nidānavagga-aṭṭhakathā',
              path: 's2a',
            },
            {
              name: 'khandhavagga-aṭṭhakathā',
              path: 's3a',
            },
            {
              name: 'saḷāyatanavagga-aṭṭhakathā',
              path: 's4a',
            },
            {
              name: 'mahāvagga-aṭṭhakathā',
              path: 's5a',
            },
          ],
        },
        k: {
          name: 'Khuddaka',
          books: [
            {
              name: 'khuddakapāṭha-aṭṭhakathā',
              path: 'k1a',
            },
            {
              name: 'dhammapada-aṭṭhakathā',
              path: 'k2a',
            },
            {
              name: 'udāna-aṭṭhakathā',
              path: 'k3a',
            },
            {
              name: 'itivuttaka-aṭṭhakathā',
              path: 'k4a',
            },
            {
              name: 'suttanipāta-aṭṭhakathā',
              path: 'k5a',
            },
            {
              name: 'vimānavatthu-aṭṭhakathā',
              path: 'k6a',
            },
            {
              name: 'petavatthu-aṭṭhakathā',
              path: 'k7a',
            },
            {
              name: 'theragāthā-aṭṭhakathā',
              path: 'k8a',
            },
            {
              name: 'therīgāthā-aṭṭhakathā',
              path: 'k9a',
            },
            {
              name: 'apadāna-aṭṭhakathā',
              path: 'k10a',
            },
            {
              name: 'buddhavaṃsa-aṭṭhakathā',
              path: 'k12a',
            },
            {
              name: 'cariyāpiṭaka-aṭṭhakathā',
              path: 'k13a',
            },
            {
              name: 'jātaka-aṭṭhakathā',
              path: 'k14a',
            },
            {
              name: 'jātaka-aṭṭhakathā',
              path: 'k15a',
            },
          ],
        },
        d: {
          name: 'Dīgha',
          books: [
            {
              name: 'sīlakkhandhavaggaṭṭhakathā',
              path: 'd1a',
            },
            {
              name: 'mahāvaggaṭṭhakathā',
              path: 'd2a',
            },
            {
              name: 'pāthikavaggaṭṭhakathā',
              path: 'd3a',
            },
          ],
        },
        m: {
          name: 'Majjhima',
          books: [
            {
              name: 'mūlapaṇṇāsa-aṭṭhakathā',
              path: 'm1a',
            },
            {
              name: 'majjhimapaṇṇāsa-aṭṭhakathā',
              path: 'm2a',
            },
            {
              name: 'uparipaṇṇāsa-aṭṭhakathā',
              path: 'm3a',
            },
          ],
        },
        x: {
          name: 'Vism',
          books: [
            {
              name: '(paṭhamo bhāgo)',
              path: 'x1a',
            },
            {
              name: '(dutiyo bhāgo)',
              path: 'x2a',
            },
          ],
        },
      },
    },
    m: {
      name: 'Mūla',
      description: 'Tipiṭaka',
      sets: {
        k: {
          name: 'Khuddaka',
          books: [
            {
              name: 'khuddakapāṭhapāḷi',
              path: 'k1m',
            },
            {
              name: 'dhammapadapāḷi',
              path: 'k2m',
            },
            {
              name: 'udānapāḷi',
              path: 'k3m',
            },
            {
              name: 'itivuttakapāḷi',
              path: 'k4m',
            },
            {
              name: 'suttanipātapāḷi',
              path: 'k5m',
            },
            {
              name: 'vimānavatthupāḷi',
              path: 'k6m',
            },
            {
              name: 'petavatthupāḷi',
              path: 'k7m',
            },
            {
              name: 'theragāthāpāḷi',
              path: 'k8m',
            },
            {
              name: 'therīgāthāpāḷi',
              path: 'k9m',
            },
            {
              name: 'therāpadānapāḷi',
              path: 'k10m',
            },
            {
              name: 'therīapadānapāḷi',
              path: 'k11m',
            },
            {
              name: 'buddhavaṃsapāḷi',
              path: 'k12m',
            },
            {
              name: 'cariyāpiṭakapāḷi',
              path: 'k13m',
            },
            {
              name: 'jātakapāḷi (paṭhamo bhāgo)',
              path: 'k14m',
            },
            {
              name: 'jātakapāḷi (dutiyo bhāgo)',
              path: 'k15m',
            },
            {
              name: 'mahāniddesapāḷi',
              path: 'k16m',
            },
            {
              name: 'cūḷaniddesapāḷi',
              path: 'k17m',
            },
            {
              name: 'paṭisambhidāmaggapāḷi',
              path: 'k18m',
            },
            {
              name: 'milindapañhapāḷi',
              path: 'k19m',
            },
            {
              name: 'nettippakaraṇapāḷi',
              path: 'k20m',
            },
            {
              name: 'peṭakopadesapāḷi',
              path: 'k21m',
            },
          ],
        },
        a: {
          name: 'Aṅguttara',
          books: [
            {
              name: 'ekakanipātapāḷi',
              path: 'a1m',
            },
            {
              name: 'dukanipātapāḷi',
              path: 'a2m',
            },
            {
              name: 'tikanipātapāḷi',
              path: 'a3m',
            },
            {
              name: 'catukkanipātapāḷi',
              path: 'a4m',
            },
            {
              name: 'pañcakanipātapāḷi',
              path: 'a5m',
            },
            {
              name: 'chakkanipātapāḷi',
              path: 'a6m',
            },
            {
              name: 'sattakanipātapāḷi',
              path: 'a7m',
            },
            {
              name: 'aṭṭhakanipātapāḷi',
              path: 'a8m',
            },
            {
              name: 'navakanipātapāḷi',
              path: 'a9m',
            },
            {
              name: 'dasakanipātapāḷi',
              path: 'a10m',
            },
            {
              name: 'ekādasakanipātapāḷi',
              path: 'a11m',
            },
          ],
        },
        b: {
          name: 'Abhidh-s',
          books: [
            {
              name: 'Abhidhammatthasaṅgaho',
              path: 'b1m',
            },
            {
              name: 'Abhidhammatthavibhāvinīṭīkā',
              path: 'b2m',
            },
          ],
        },
        y: {
          name: 'Abhidhamma',
          books: [
            {
              name: 'dhammasaṅgaṇīpāḷi',
              path: 'y1m',
            },
            {
              name: 'vibhaṅgapāḷi',
              path: 'y2m',
            },
            {
              name: 'dhātukathāpāḷi',
              path: 'y3m',
            },
            {
              name: 'puggalapaññattipāḷi',
              path: 'y4m',
            },
            {
              name: 'kathāvatthupāḷi',
              path: 'y5m',
            },
            {
              name: 'yamakapāḷi (paṭhamo bhāgo)',
              path: 'y6m',
            },
            {
              name: 'yamakapāḷi (dutiyo bhāgo)',
              path: 'y7m',
            },
            {
              name: 'yamakapāḷi (tatiyo bhāgo)',
              path: 'y8m',
            },
            {
              name: 'paṭṭhānapāḷi (paṭhamo bhāgo) dhammānulome tikapaṭṭhānaṃ',
              path: 'y9m',
            },
            {
              name: 'paṭṭhānapāḷi (dutiyo bhāgo) dhammānulome tikapaṭṭhānaṃ',
              path: 'y10m',
            },
            {
              name: 'paṭṭhānapāḷi  (tatiyo bhāgo) dhammānulome dukapaṭṭhānaṃ',
              path: 'y11m',
            },
            {
              name: 'paṭṭhānapāḷi (catuttho bhāgo) dhammānulome dukapaṭṭhānaṃ',
              path: 'y12m',
            },
            {
              name: 'paṭṭhānapāḷi (catuttho bhāgo)',
              path: 'y13m',
            },
            {
              name: 'paṭṭhānapāḷi (pañcamo bhāgo)',
              path: 'y14m',
            },
          ],
        },
        g: {
          name: 'Byākaraṇa',
          books: [
            {
              name: 'moggallāna',
              path: 'g1m',
            },
            {
              name: 'kaccāyanabyākaraṇaṃ',
              path: 'g2m',
            },
            {
              name: 'saddanītippakaraṇaṃ (padamālā)',
              path: 'g3m',
            },
            {
              name: 'Saddanītippakaraṇaṃ Dhātumālā',
              path: 'g4m',
            },
            {
              name: 'padarūpasiddhi',
              path: 'g5m',
            },
          ],
        },
        v: {
          name: 'Vinaya',
          books: [
            {
              name: 'pārājikapāḷi',
              path: 'v1m',
            },
            {
              name: 'pācittiyapāḷi',
              path: 'v2m',
            },
            {
              name: 'bhikkhunīvibhaṅgo',
              path: 'v3m',
            },
            {
              name: 'mahāvaggapāḷi',
              path: 'v4m',
            },
            {
              name: 'cūḷavaggapāḷi',
              path: 'v5m',
            },
            {
              name: 'parivārapāḷi',
              path: 'v6m',
            },
          ],
        },
        s: {
          name: 'Saṃyutta',
          books: [
            {
              name: 'sagāthāvaggo',
              path: 's1m',
            },
            {
              name: 'nidānavaggo',
              path: 's2m',
            },
            {
              name: 'khandhavaggo',
              path: 's3m',
            },
            {
              name: 'saḷāyatanavaggo',
              path: 's4m',
            },
            {
              name: 'mahāvaggo',
              path: 's5m',
            },
          ],
        },
        d: {
          name: 'Dīgha',
          books: [
            {
              name: 'sīlakkhandhavaggapāḷi',
              path: 'd1m',
            },
            {
              name: 'mahāvaggapāḷi',
              path: 'd2m',
            },
            {
              name: 'pāthikavaggapāḷi',
              path: 'd3m',
            },
          ],
        },
        x: {
          name: 'Vism',
          books: [
            {
              name: '(paṭhamo bhāgo)',
              path: 'x1m',
            },
            {
              name: '(dutiyo bhāgo)',
              path: 'x2m',
            },
          ],
        },
        m: {
          name: 'Majjhima',
          books: [
            {
              name: 'mūlapaṇṇāsapāḷi',
              path: 'm1m',
            },
            {
              name: 'majjhimapaṇṇāsapāḷi',
              path: 'm2m',
            },
            {
              name: 'uparipaṇṇāsapāḷi',
              path: 'm3m',
            },
          ],
        },
      },
    },
    t: {
      name: 'Tīkā ',
      description: 'Subcommentaries',
      sets: {
        a: {
          name: 'Aṅguttara',
          books: [
            {
              name: 'Ekakanipāta-ṭīkā',
              path: 'a1t',
            },
            {
              name: 'Dukanipāta-ṭīkā',
              path: 'a2t',
            },
            {
              name: 'Tikanipāta-ṭīkā',
              path: 'a3t',
            },
            {
              name: 'Catukkanipāta-ṭīkā',
              path: 'a4t',
            },
            {
              name: 'Pañcakanipāta-ṭīkā',
              path: 'a5t',
            },
            {
              name: 'Chakkanipāta-ṭīkā',
              path: 'a6t',
            },
            {
              name: 'Sattakanipāta-ṭīkā',
              path: 'a7t',
            },
            {
              name: 'Aṭṭhakanipāta-ṭīkā',
              path: 'a8t',
            },
            {
              name: 'Navakanipāta-ṭīkā',
              path: 'a9t',
            },
            {
              name: 'Dasakanipāta-ṭīkā',
              path: 'a10t',
            },
            {
              name: 'Ekādasakanipāta-ṭīkā',
              path: 'a11t',
            },
          ],
        },
        v: {
          name: 'Vinaya',
          books: [
            {
              name: 'Sāratthadīpanī-ṭīkā',
              path: 'v1t',
            },
            {
              name: 'Sāratthadīpanī-ṭīkā (tatiyo bhāgo)',
              path: 'v2t',
            },
            {
              name: 'Sāratthadīpanī-ṭīkā',
              path: 'v3t',
            },
            {
              name: 'Sāratthadīpanī-ṭīkā Mahāvagga-ṭīkā',
              path: 'v4t',
            },
            {
              name: 'Sāratthadīpanī-ṭīkā Cūḷavagga-ṭīkā',
              path: 'v5t',
            },
            {
              name: 'Sāratthadīpanī-ṭīkā Parivāra-ṭīkā',
              path: 'v6t',
            },
            {
              name: 'Dvemātikāpāḷi',
              path: 'v7t',
            },
            {
              name: 'Vinayasaṅgaha-aṭṭhakathā',
              path: 'v8t',
            },
            {
              name: 'Vajirabuddhi-ṭīkā',
              path: 'v9t',
            },
            {
              name: 'Vimativinodanī-ṭīkā',
              path: 'v10t',
            },
            {
              name: 'Vinayālaṅkāra-ṭīkā',
              path: 'v11t',
            },
          ],
        },
        y: {
          name: 'Abhidhamma',
          books: [
            {
              name: 'Dhammasaṅgaṇī-mūlaṭīkā',
              path: 'y1t',
            },
            {
              name: 'Vibhaṅga-mūlaṭīkā',
              path: 'y2t',
            },
            {
              name: 'Dhātukathāpakaraṇa-mūlaṭīkā',
              path: 'y3t',
            },
            {
              name: 'Puggalapaññattipakaraṇa-mūlaṭīkā',
              path: 'y4t',
            },
            {
              name: 'Kathāvatthupakaraṇa-mūlaṭīkā',
              path: 'y5t',
            },
            {
              name: 'Yamakapakaraṇa-mūlaṭīkā',
              path: 'y6t',
            },
            {
              name: 'Paṭṭhānapakaraṇa-mūlaṭīkā',
              path: 'y9t',
            },
          ],
        },
        s: {
          name: 'Saṃyutta',
          books: [
            {
              name: 'Sagāthāvaggaṭīkā',
              path: 's1t',
            },
            {
              name: 'Nidānavaggaṭīkā',
              path: 's2t',
            },
            {
              name: 'Khandhavaggaṭīkā',
              path: 's3t',
            },
            {
              name: 'Saḷāyatanavaggaṭīkā',
              path: 's4t',
            },
            {
              name: 'Mahāvaggaṭīkā',
              path: 's5t',
            },
          ],
        },
        m: {
          name: 'Majjhima',
          books: [
            {
              name: 'Mūlapaṇṇāsa-ṭīkā',
              path: 'm1t',
            },
            {
              name: 'Majjhimapaṇṇāsaṭīkā',
              path: 'm2t',
            },
            {
              name: 'Uparipaṇṇāsa-ṭīkā',
              path: 'm3t',
            },
          ],
        },
        d: {
          name: 'Dīgha',
          books: [
            {
              name: 'Sīlakkhandhavaggaṭīkā',
              path: 'd1t',
            },
            {
              name: 'Mahāvaggaṭīkā',
              path: 'd2t',
            },
            {
              name: 'Pāthikavaggaṭīkā',
              path: 'd3t',
            },
          ],
        },
      },
    },
  },
};
