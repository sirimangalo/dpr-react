import 'fake-indexeddb/auto';
import Tipitaka from './index';
import Search from './search';
import Store from './store';

import mockBook from './test_data/mock_book';
import { TipitakaSet, TipitakaHeir, TipitakaNikaya } from './interfaces';

jest.mock('./meta/myanmar', () => require('./test_data/mock_meta'));
jest.mock('./store');
jest.mock('./search');

// @ts-ignore
Store.mockImplementation(() => ({
  get: jest.fn(() => mockBook),
}));

// @ts-ignore
Search.mockImplementation(() => ({
  search: jest.fn(() => 'search'),
  regexSearch: jest.fn(() => 'regexSearch'),
}));

describe('Tipitaka', () => {
  it('should preload', async () => {
    const tipitaka = new Tipitaka(TipitakaSet.myanmar);
    await tipitaka.preload();
  });

  it('should get section', async () => {
    const tipitaka = new Tipitaka(TipitakaSet.myanmar);

    const section = await tipitaka.getSection({ hier: TipitakaHeir.a, nikaya: TipitakaNikaya.a });

    expect(section).toEqual([
      {
        paragraphs: [{ type: 1, body: 'the quick brown fox' }, { type: 2, body: 'lorem ipsum' }],
        displayLocation: ['mock meta', 'mock volume', 'mock vagga', 'mock sutta 1', 'mock section 1'],
        location: { nikaya: 'a', hier: 'a', book: 0, meta: 0, volume: 0, vagga: 0, sutta: 0, section: 0 },
      },
      {
        paragraphs: [{ type: 3, body: 'hello world' }, { type: 4, body: 'foo bar' }],
        displayLocation: ['mock meta', 'mock volume', 'mock vagga', 'mock sutta 2', 'mock section 2'],
        location: { nikaya: 'a', hier: 'a', book: 0, meta: 0, volume: 0, vagga: 0, sutta: 1, section: 0 },
      },
    ]);
  });

  it('should get navigation', async () => {
    const tipitaka = new Tipitaka(TipitakaSet.myanmar);

    const navigation = await tipitaka.getNavigation({ hier: TipitakaHeir.a, nikaya: TipitakaNikaya.a });

    expect(navigation).toEqual({
      nikaya: ['Aṅguttara'],
      book: ['ekakanipāta-aṭṭhakathā'],
      meta: ['mock meta'],
      volume: ['mock volume'],
      vagga: ['mock vagga'],
      sutta: ['mock sutta 1', 'mock sutta 2'],
      section: ['mock section 1'],
    });
  });

  it('should search ', async () => {
    const tipitaka = new Tipitaka(TipitakaSet.myanmar);

    expect(await tipitaka.search('', false)).toEqual('search');
    expect(await tipitaka.search('', true)).toEqual('regexSearch');
  });
});
