# digital-pali-reader

Link to old version: https://github.com/yuttadhammo/digitalpalireader

# Installation

> NOTE:
>
> 1. Install packages listed [here](./CONTRIBUTING.md#Tools) for optimum development experience.
> 2. For a React primer, refer to [this tutorial](https://gitlab.com/snippets/1883405).

## Production build

`yarn build`

## Lint

`yarn lint`

> NOTE: If you have installed the packages listed [here](./CONTRIBUTING.md#Tools), the lint errors will appear in VSCode itself.

## Test

Two options:

1. yarn run test # Command watches the file activity and reruns 'affected' tests only.
2. yarn run citest # For a one time test run

## Run

`yarn start`

> NOTE: This will start the server in the background and also do HMR.

## Debug

1. First [run](#Run) the application.
2. Then you have two options:
    1. Using chrome developer tools (ensure you install all tools from CONTRIBUTING.md)
    2. F5 from VSCode (start VSCode from project root folder before that)
