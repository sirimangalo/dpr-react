<!--

Thank you for submitting a bug report.

Please search open and closed issues with your request before submitting a new one.
Existing issues often contain information about workarounds, resolution, or progress updates.

Please also carefully read the ✍️ sections.

-->

# 🐞 Bug Report

### Description
<!--

✍️ Please give a clear and concise description about the bug. What was the expected behaviour and what happened instead? Please provide detailed steps to reproduce the issue.

-->


### Exception or error
<pre><code>
<!-- ✍️ If you got an error message please share it below. Otherwise delete this section. -->

</code></pre>


### Your Environment
<!-- ✍️ Please fill the details below -->

**Browser and its version:**

**Operating system:**

### Anything else relevant?
<!-- ✍️ Screenshots, attachments, etc. can go here. -->
